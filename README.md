 Esta es algo de la estructura a utilizar para mi proyecto aunque la sigo modificando pero doy a mostrar un poco de como estara siendo administrado mi codigo de igual forma algunos estilos.
  Asi se estara aplicando normalize para una mejor visualizacion y el responsive web design.
  
  :root{
    --blanco: #ffffff;
    --oscuro: #212121;
    --primario: #FFC107;
    --secundario: #0097A7;
    --gris: #757575;
}
html{
    font-size: 62.5%;
}
body{
    font-size: 16px;
    font-family: 'Krub', sans-serif;
}

h1 {
    text-align: center;
    font-size: 3rem;
    
    }

span{
    font-size: 2rem;
}
.nav-bg{
    background-color: var(--secundario);

}

.navPosicion{
   max-width: 120rem;
   margin-top: 0 auto;
}

.principal-nav{
    display: flex;
    flex-direction: column;
     
    
}
@media (min-Width: 480px) {
    .principal-nav{
        flex-direction: row;
        justify-content: space-between;
    }
    
}
.principal-nav a{
    display: block;
    text-align: center;
    color: var(--blanco);
    text-decoration: none;
    font-size: 2rem;
    font-weight:bold;
    padding: 1rem;
    
}
.principal-nav a:hover{
    background-color: var(--primario);
    color: var(--oscuro);
}
.hero{
    background-image: url("../imagenes/hero.jpg");
    background-repeat: no-repeat;
    background-size: cover;
}
 
    